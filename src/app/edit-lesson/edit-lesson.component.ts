import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Lesson } from '../shared/model/lesson.model';
import { LessonsService } from '../shared/model/lessons.service';

@Component({
  selector: 'app-edit-lesson',
  templateUrl: './edit-lesson.component.html',
  styleUrls: ['./edit-lesson.component.sass'],
})
export class EditLessonComponent implements OnInit {
  lesson: Lesson;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private lessonService: LessonsService,
  ) {}

  ngOnInit() {
    this.route.data.subscribe(data => (this.lesson = data['lesson']));
  }

  save(lesson: Lesson) {
    this.lessonService
      .saveLessson(this.lesson['$key'], lesson)
      .subscribe(
        () => this.router.navigateByUrl(`lessons/${lesson.url}`),
        err => console.error(err),
      );
  }
}
