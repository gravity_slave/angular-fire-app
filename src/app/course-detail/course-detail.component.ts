import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../shared/model/courses.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Course } from '../shared/model/course.model';
import { Lesson } from '../shared/model/lesson.model';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.sass'],
})
export class CourseDetailComponent implements OnInit {
  course$: Observable<Course>;
  lessons: Lesson[];
  courseUrl: string;

  constructor(
    private courseService: CoursesService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit() {
    this.courseUrl = this.route.snapshot.params['id'];
    this.course$ = this.courseService
      .findCourseByUrl(this.courseUrl)
      .snapshotChanges()
      .map(arr => arr[0])
      .map(ref => {
        const ret = ref['payload'].val();
        ret['$key'] = ref['key'];
        return ret;
      });
    const lessons$ = this.courseService.loadFirstLessons(this.courseUrl, 3);
    const lessonsSubscribtion = lessons$.subscribe(
      lessons => (this.lessons = lessons),
      err => console.log(err),
      () => lessonsSubscribtion.unsubscribe(),
    );
  }

  next() {
    const nextLessonsSubscription = this.courseService
      .loadNextPages(
        this.courseUrl,
        this.lessons[this.lessons.length - 1]['$key'],
        3,
      )
      .subscribe(
        lessons => (this.lessons = lessons),
        err => console.log(err),
        () => nextLessonsSubscription.unsubscribe(),
      );
    // .subscribe(lessons => this.lessons = lessons)
  }

  previous() {
    const previousLessonsSub = this.courseService
      .loadPreviousPages(this.courseUrl, this.lessons[0]['$key'], 3)
      .subscribe(
        lessons => (this.lessons = lessons),
        err => console.log(err),
        () => previousLessonsSub.unsubscribe(),
      );
  }

  navigateToLesson(lesson: Lesson) {
    this.router.navigateByUrl(`lessons/${lesson.url}`);
  }
}
