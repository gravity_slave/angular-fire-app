import { Component, OnInit } from '@angular/core';
import { CoursesService } from '../shared/model/courses.service';
import { Course } from '../shared/model/course.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.sass'],
})
export class CoursesComponent implements OnInit {
  courses$: Observable<Course[]>;

  constructor(private courseService: CoursesService) {}

  ngOnInit() {
    this.courses$ = this.courseService.findAllCourses();
  }
}
