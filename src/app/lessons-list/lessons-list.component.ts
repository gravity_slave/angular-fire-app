import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Lesson } from '../shared/model/lesson.model';

@Component({
  selector: 'app-lessons-list',
  templateUrl: './lessons-list.component.html',
  styleUrls: ['./lessons-list.component.sass'],
})
export class LessonsListComponent implements OnInit {
  @Output() lesson: EventEmitter<Lesson> = new EventEmitter();
  @Input() lessons: Lesson[];
  constructor() {}

  ngOnInit() {}

  emitLesson(lesson: Lesson) {
    this.lesson.emit(lesson);
  }
}
