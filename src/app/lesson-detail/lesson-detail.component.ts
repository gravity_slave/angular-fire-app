import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { LessonsService } from '../shared/model/lessons.service';
import { Lesson } from '../shared/model/lesson.model';

@Component({
  selector: 'app-lesson-detail',
  templateUrl: './lesson-detail.component.html',
  styleUrls: ['./lesson-detail.component.sass'],
})
export class LessonDetailComponent implements OnInit {
  lesson: Lesson;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private lessonsService: LessonsService,
  ) {}

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        return this.lessonsService.findLessonByUrl(params.get('id'));
      })
      .subscribe(lesson => (this.lesson = lesson));
  }

  next() {
    this.lessonsService
      .loadNextLesson(this.lesson.courseId, this.lesson['$key'])
      .subscribe(this.navigateToLesson.bind(this));
  }

  previous() {
    this.lessonsService
      .loadPreviousLesson(this.lesson.courseId, this.lesson.$key)
      .subscribe(this.navigateToLesson.bind(this));
  }

  navigateToLesson(lesson: Lesson) {
    this.router.navigateByUrl(`/lessons/${lesson.url}`);
  }

  delete() {
    // this.lessonsService
    //   .deleteLesson(this.lesson.$key)
    //   .subscribe(() => alert('Lesson deleted'), console.error);
  }

  requestLessonDeletion() {
    // this.lessonsService.requestLessonDeletion(
    //   this.lesson.$key,
    //   this.lesson.courseId,
    // );
  }
}
