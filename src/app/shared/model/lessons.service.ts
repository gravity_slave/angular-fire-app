import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Lesson } from './lesson.model';
import { AngularFireDatabase } from 'angularfire2/database';
import { FirebaseApp } from 'angularfire2';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LessonsService {
  sdkdb: any;
  constructor(
    private af: AngularFireDatabase,
    @Inject(FirebaseApp) fb,
    private http: HttpClient,
  ) {
    this.sdkdb = fb.database().ref();
  }

  findAllLessons(): Observable<Lesson[]> {
    // return this.af.collection('lessons').valueChanges().map( res => res as Lesson[]);
    return this.af
      .list('lessons')
      .valueChanges()
      .map(res => res as Lesson[]);
  }

  findLessonByUrl(url: string): Observable<Lesson> {
    return this.af
      .list('lessons', ref => ref.orderByChild('url').equalTo(url))
      .snapshotChanges()
      .filter(results => results && results.length > 0)
      .map(arr => {
        const val = arr[0]['payload'].val();
        val['$key'] = arr[0]['key'];
        return val;
      });
  }

  loadNextLesson(courseId, lessonId: string): Observable<Lesson> {
    return this.af
      .list(`lessonsPerCourse/${courseId}`, ref =>
        ref
          .startAt(lessonId)
          .orderByKey()
          .limitToFirst(2),
      )
      .snapshotChanges()
      .map(res => res[1]['key'])
      .switchMap(lesson =>
        this.af.object(`lessons/${lesson}`).snapshotChanges(),
      )
      .map(res => {
        const ret = res['payload'].val();
        ret['$key'] = res['key'];
        return ret;
      })
      .map(res => res as Lesson);
  }

  loadPreviousLesson(courseId, lessonId: string): Observable<any> {
    return this.af
      .list(`lessonsPerCourse/${courseId}`, ref =>
        ref
          .endAt(lessonId)
          .orderByKey()
          .limitToLast(2),
      )
      .snapshotChanges()
      .map(res => res[0]['key'])
      .switchMap(lesson =>
        this.af.object(`lessons/${lesson}`).snapshotChanges(),
      )
      .map(res => {
        const ret = res['payload'].val();
        ret['$key'] = res['key'];
        return ret;
      })
      .map(res => res as Lesson);
  }

  createNewLesson(courseId: string, lesson: any): Observable<any> {
    const newLesson = Object.assign({}, lesson, { courseId });
    const newLessonKey = this.sdkdb.child('lessons').push().key;
    const dataToSave = {};

    dataToSave['lessons/' + newLessonKey] = newLesson;
    dataToSave[`lessonsPerCourse/${courseId}/${newLessonKey}`] = true;

    return this.firebaseUpdate(dataToSave);
  }

  firebaseUpdate(dataToSave) {
    const subject = new Subject();

    this.sdkdb.update(dataToSave).then(
      val => {
        subject.next(val);
        subject.complete();
      },
      err => {
        subject.error(err);
        subject.complete();
      },
    );

    return subject.asObservable();
  }

  saveLessson(lessonId: string, lesson: Lesson): Observable<any> {
    const lessonToSave = Object.assign({}, lesson);
    delete lessonToSave['$key'];

    const dataToSave = {};
    dataToSave[`lessons/${lessonId}`] = lessonToSave;
    return this.firebaseUpdate(dataToSave);
  }

  deleteLesson() {}

  requestLessonDeletion() {}
}
