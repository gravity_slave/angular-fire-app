import { Injectable } from '@angular/core';
import { AngularFireDatabase, QueryFn } from 'angularfire2/database';
import { Course } from './course.model';
import { Observable } from 'rxjs/Observable';
import { Lesson } from './lesson.model';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/pluck';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/filter';

import { combineLatest } from 'rxjs/observable/combineLatest';

@Injectable()
export class CoursesService {
  constructor(private af: AngularFireDatabase) {}

  findAllCourses(): Observable<Course[]> {
    return this.af
      .list('courses')
      .valueChanges()
      .map(res => res as Course[]);
  }
  //
  // findLessonByUrl(url: string): Observable<Lesson> {
  //   return this.af
  //     .list('courses', ref => ref.orderByChild('url').equalTo(url))
  //     .valueChanges()
  //     .filter(results => results && results.length > 0)
  //     .map(lesson => lesson[0] as Lesson);
  // }

  findCourseByUrl(url: string): any {
    return this.af.list('courses', ref => ref.orderByChild('url').equalTo(url));
  }

  findLessonKeysPerCourseUrl(
    courseUrl: string,
    query: QueryFn = ref => ref,
  ): Observable<string[]> {
    return this.findCourseByUrl(courseUrl)
      .snapshotChanges()
      .switchMap(course => {
        return this.af
          .list(`lessonsPerCourse/${course[0]['key']}`, query)
          .snapshotChanges()
          .map(arr => arr.map(val => val['key']));
      });
  }

  findLessonsForLessonKeys(
    lessonKeys$: Observable<string[]>,
  ): Observable<Lesson[]> {
    return lessonKeys$
      .map(arr =>
        arr.map(val =>
          this.af
            .object(`lessons/${val}`)
            .valueChanges()
            .map(vall => {
              vall['$key'] = val;
              return vall;
            }),
        ),
      )
      .mergeMap(obs => combineLatest(obs))
      .map(arr => <Lesson[]>arr);
  }

  findAllLessonsForCourse(courseUrl: string): Observable<Lesson[]> {
    return this.findLessonsForLessonKeys(
      this.findLessonKeysPerCourseUrl(courseUrl),
    );
  }

  loadFirstLessons(courseUrl: string, pagesize: number): Observable<Lesson[]> {
    const lessonKeys$ = this.findLessonKeysPerCourseUrl(courseUrl, ref =>
      ref.limitToFirst(pagesize),
    );
    return this.findLessonsForLessonKeys(lessonKeys$);
  }

  loadNextPages(
    courseUrl: string,
    lastLesson: string,
    pagesize: number = 3,
  ): Observable<Lesson[]> {
    const lessonKeys$ = this.findLessonKeysPerCourseUrl(courseUrl, ref =>
      ref
        .orderByKey()
        .startAt(lastLesson)
        .limitToFirst(pagesize + 1)
    ).map(lessons => lessons.slice(1, lessons.length));
    return this.findLessonsForLessonKeys(lessonKeys$);
  }

  loadPreviousPages(
    courseUrl: string,
    lastLesson: string,
    pagesize: number = 3,
  ): Observable<Lesson[]> {
      const lessonKeys$ = this.findLessonKeysPerCourseUrl(courseUrl, ref =>
          ref
              .orderByKey()
              .endAt(lastLesson)
              .limitToLast(pagesize + 1)
      ).map(lessons => lessons.slice(0, lessons.length - 1));
      return this.findLessonsForLessonKeys(lessonKeys$).do(console.log);  }
}
