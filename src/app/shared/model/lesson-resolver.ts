import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { LessonsService } from './lessons.service';
import { Lesson } from './lesson.model';
import 'rxjs/add/operator/first'

@Injectable()
export class LessonResolver implements Resolve<Lesson> {
  constructor(private lessonService: LessonsService) {}
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ): Observable<any> {
    return this.lessonService.findLessonByUrl(route.params['id']).first();
  }
}
