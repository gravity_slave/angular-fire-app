import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {
  authState: any = null;
  constructor(private afAuth: AngularFireAuth) {}

  login(email, password: string): Observable<any> {
    const promise = new Subject<any>();
    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        promise.next(res);
        promise.complete();
      })
      .catch(err => {
        promise.next();
        promise.complete();
      });

    return promise.asObservable();
  }
}
